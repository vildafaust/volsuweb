var express          =   require('express'); //SERVEr
var app              =   express(); //SERVER

var path             =   require('path'); //пути к дирикториям

var morgan           =  require('morgan');

var cookieParser     =  require('cookie-parser');

var bodyParser       =  require('body-parser');
app.use(bodyParser.json());

var session          =  require('express-session');

var mongoose         =   require('mongoose');
var mongo            =   require("mongodb");

var passport         =   require('passport');
var flash            = require('connect-flash')

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
const fileUpload = require('express-fileupload');
//app.use(fileUpload());

//
// конфигурация




require('./config/passport')(passport);

app.use(express.static('./'));

app.use(morgan('dev'));
app.use(cookieParser());
app.use(express.urlencoded({ extended: false }));

app.use(passport.initialize());
app.use(passport.session());


app.use('/static', express.static(__dirname + '/public'));

app.use(flash());
require('./app/routes.js')(app,passport);

mongoose.connect('mongodb://root:O3oSJ8ShDUJpbWRW@tempname-shard-00-00-bq4zo.mongodb.net:27017,tempname-shard-00-01-bq4zo.mongodb.net:27017,tempname-shard-00-02-bq4zo.mongodb.net:27017/test?ssl=true&replicaSet=TempName-shard-0&authSource=admin&retryWrites=true');

app.set('view engine', 'ejs');

app.listen(33, function () {
    console.log('Соединение установлено. Порт ' );
});
