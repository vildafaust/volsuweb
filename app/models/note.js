var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

var noteSchema = mongoose.Schema({
    id:Number,
    day: String,
    state: Boolean,
});



module.exports = mongoose.model('note', noteSchema);
