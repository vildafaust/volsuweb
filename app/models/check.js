var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

var checkSchema = mongoose.Schema({
    id:Number,
    nameGroup:String,
    nameHost:String,
    listCheck: [],
    listUnchecked: [],
    state: Boolean,

});



module.exports = mongoose.model('check', checkSchema);
