var express  = require('express');
var router   = express.Router();
var ejs      = require ('ejs');
var mongoose = require("mongoose");
mongoose.Promise = Promise;

var Users = require('../app/models/user.js');
var Note = require('../app/models/note.js');
var Check = require('../app/models/check.js');
var ListGroups = require('../app/models/listgroups.js');
const jsonParser = express.json();

module.exports = function (app, passport) {

    app.get('/', function (req, res) {
            res.render('index.ejs');
    });

    app.route('/adminpanel')
        .get(function(req,res) {
          ListGroups.find({}, function(err, data){
            console.log(data);
            res.render("adminpanel.ejs", {list:data})
          });

        })
        .post(function(req,res) {
          console.log(JSON.stringify(req.body.title));

          var newListGroup = new ListGroups();
          newListGroup.nameGroups = req.body.title
          newListGroup.save(function(err){
          if(err) return console.log(err);
          console.log("Сохранен объект Order", newListGroup);
          res.json(req.body.title)
         });
});
    app.post("addgroup",function(req,res){

      res.redirict("/adminpanel")
    })

    app.get('/404', function(req, res){
        res.redirect('/');
    });

    function isLoggedIn(req) {
        return req.isAuthenticated();
    }
    function checkPerm(req){
      if (isLoggedIn(req)){
        role = req.user.perm
      } else {
        role = "guest"
      }
      return role
    }

};
